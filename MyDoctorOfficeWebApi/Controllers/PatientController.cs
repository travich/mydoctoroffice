﻿using MyDoctorOffice.Models;
using MyDoctorOfficeWebAPI.Services;
using Ninject;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace MyDoctorOfficeWebApi.Controllers
{

    //[Authorize]
    public class PatientController : ApiController
    {
        [Inject]
        private readonly IPatientService _patientService;

        public PatientController(IPatientService patientService) {
            _patientService = patientService;
        }

        // GET api/values
        public IEnumerable<PatientModel> Get()
        {
            return _patientService.GetPatients();
        }

        // GET api/values/5
        public PatientModel Get(int id)
        {
            return _patientService.GetPatient(id);
        }

        // POST api/values
        public void Post(PatientModel patient)
        {
            _patientService.CreatePatient(patient);
        }

        // PUT api/values/5
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE api/values/5
        public void Delete(int id)
        {
        }
    }
}
