﻿using MyDoctorOfficeWebApi.Ef;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MyDoctorOfficeWebApi.Models
{
    public class DoctorModel
    {
        public int DoctorId { get; set; }
        public string DoctorName { get; set; }
        public List<Patient> Patients { get; set; }
    }
}