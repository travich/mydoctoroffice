﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using MyDoctorOffice.Models;
using MyDoctorOfficeWebApi.Ef;

namespace MyDoctorOfficeWebAPI.Repositories
{
    public class PatientRepository : IPatientRepository
    {
        public void CreatePatient(PatientModel patientModel)
        {
            using (var context = new MyDoctorOfficeDbContext())
            {
                context.Patients.Add(new Patient
                {
                    Birthdate = patientModel.Birthdate,
                    EmailAddress = patientModel.EmailAddress,
                    FullName = patientModel.FullName,
                    PhoneNumber = patientModel.PhoneNumber,
                    ReasonForVisit = patientModel.ReasonForVisit
                });
                context.SaveChanges();
            }
        }

        public List<PatientModel> GetPatients()
        {
            using (var context = new MyDoctorOfficeDbContext())
            {
                var patients = context.Patients;
                var patientsModel = new List<PatientModel>();
                patients.ToList().ForEach(p => patientsModel.Add(new PatientModel
                {
                    Birthdate = p.Birthdate,
                    EmailAddress = p.EmailAddress,
                    FullName = p.FullName,
                    PatientId = p.PatientId,
                    PhoneNumber = p.PhoneNumber,
                    ReasonForVisit = p.ReasonForVisit
                }));

                return patientsModel;
            }
        }

        public PatientModel GetPatient(int id)
        {
            using (var context = new MyDoctorOfficeDbContext())
            {
                var patient = context.Patients.Where(p => p.PatientId == id).FirstOrDefault();
                var patientModel = new PatientModel();
                patientModel = new PatientModel
                {
                    Birthdate = patient.Birthdate,
                    EmailAddress = patient.EmailAddress,
                    FullName = patient.FullName,
                    PatientId = patient.PatientId,
                    PhoneNumber = patient.PhoneNumber,
                    ReasonForVisit = patient.ReasonForVisit
                };

                return patientModel;
            }
        }
    }
}