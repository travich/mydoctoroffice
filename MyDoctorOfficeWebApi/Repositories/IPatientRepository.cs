﻿using MyDoctorOffice.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MyDoctorOfficeWebAPI.Repositories
{
    public interface IPatientRepository
    {
        void CreatePatient(PatientModel patient);
        List<PatientModel> GetPatients();
        PatientModel GetPatient(int id);
    }
}