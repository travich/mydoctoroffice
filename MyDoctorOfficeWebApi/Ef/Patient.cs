﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MyDoctorOfficeWebApi.Ef
{
    public class Patient
    {
        public int PatientId { get; set; }
        public string FullName { get; set; }
        public string EmailAddress { get; set; }
        public string PhoneNumber { get; set; }
        public DateTime Birthdate { get; set; }
        public string ReasonForVisit { get; set; }
    }
}