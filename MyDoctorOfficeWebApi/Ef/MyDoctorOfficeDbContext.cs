﻿using MyDoctorOffice.Models;
using MyDoctorOfficeWebApi.Models;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace MyDoctorOfficeWebApi.Ef
{
    public class MyDoctorOfficeDbContext : DbContext
    {
        static MyDoctorOfficeDbContext()
        {
            //Database.SetInitializer<MyDoctorOfficeDbContext>(null);
        }
        public MyDoctorOfficeDbContext()
            : base()
        {

        }

        public Database DatabaseContext
        {
            get
            {
                return base.Database;
            }
        }

        public DbSet<Patient> Patients { get; set; }
        public DbSet<Doctor> Doctors { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            //modelBuilder.Configurations.Add(new PatientMap());
            //modelBuilder.Configurations.Add(new DoctorMap());
        }

    }
}