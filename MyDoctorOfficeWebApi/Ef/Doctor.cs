﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MyDoctorOfficeWebApi.Ef
{
    public class Doctor
    {
        public int DoctorId { get; set; }
        public string DoctorName { get; set; }
        public List<Patient> Patients { get; set; }
    }
}