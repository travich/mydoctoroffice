﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using MyDoctorOffice.Models;
using MyDoctorOfficeWebAPI.Repositories;

namespace MyDoctorOfficeWebAPI.Services
{
    public class PatientService : IPatientService
    {
        private readonly IPatientRepository _patientRepository;

        public PatientService(IPatientRepository patientRepository)
        {
            _patientRepository = patientRepository;
        }

        public void CreatePatient(PatientModel patient)
        {
            _patientRepository.CreatePatient(patient);
        }

        public List<PatientModel> GetPatients()
        {
            return _patientRepository.GetPatients();
        }

        public PatientModel GetPatient(int id) {
            return _patientRepository.GetPatient(id);
        }
    }
}