﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace MyDoctorOffice.Models
{
    public class PatientModel
    {
        [Display(Name="Full Name: ")]
        [Required(ErrorMessage="Your full name is required.")]
        public string FullName { get; set; }
        [Display(Name = "Email Address: ")]
        public string EmailAddress { get; set; }
        [Required(ErrorMessage = "Your phone number is required.")]
        [Display(Name = "Phone Number: ")]
        public string PhoneNumber { get; set; }
        [Display(Name = "Birth Date: ")]
        [Required(ErrorMessage = "Your birthdate is required.")]
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:MM/dd/yyyy}")]
        public DateTime Birthdate { get; set; }
        [Display(Name = "Reason For Visit: ")]
        [Required(ErrorMessage = "Your reason for visit is required.")]
        public string ReasonForVisit { get; set; }

        public int PatientId { get; set; }
    }
}