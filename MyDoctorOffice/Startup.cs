﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(MyDoctorOffice.Startup))]
namespace MyDoctorOffice
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
