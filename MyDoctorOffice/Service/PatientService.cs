﻿using MyDoctorOffice.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Web;
using System.Configuration;
using System.Threading.Tasks;

namespace MyDoctorOffice.Service
{
    public class PatientService
    {
        readonly string url = ConfigurationManager.AppSettings["WebApi"] + "Patient/";

        public async Task CreatePatient(PatientModel patient)
        {
            var client = new HttpClient();
            var response = client.PostAsJsonAsync(url, patient).Result;
        }

        public async Task<List<PatientModel>> GetPatients()
        {
            var client = new HttpClient();
            var response = client.GetAsync(url).Result;
            if (response.IsSuccessStatusCode)
            {
                var patients = await response.Content.ReadAsAsync<IEnumerable<PatientModel>>();
                return patients.ToList();
            }

            return null;
        }

        public async Task<PatientModel> GetPatient(int patientId)
        {
            var client = new HttpClient();
            var response = client.GetAsync(url + "/" + patientId).Result;
            if (response.IsSuccessStatusCode)
            {
                var patients = await response.Content.ReadAsAsync<PatientModel>();
                return patients;
            }

            return null;
        }
    }
}