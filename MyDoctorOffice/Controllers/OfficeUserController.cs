﻿using MyDoctorOffice.Service;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace MyDoctorOffice.Controllers
{

    [Authorize(Roles = "OfficeUser")]
    public class OfficeUserController : Controller
    {
        //TODO: Use DI
        PatientService _service = new PatientService();


        public ActionResult Index()
        {
            var patients = _service.GetPatients().ConfigureAwait(false);
            return View(patients.GetAwaiter().GetResult());
        }

        [HttpGet]
        public ActionResult PatientDetail(int patientId)
        {
            var patient = _service.GetPatient(patientId).ConfigureAwait(false);
            return View(patient.GetAwaiter().GetResult());
        }
    }


}