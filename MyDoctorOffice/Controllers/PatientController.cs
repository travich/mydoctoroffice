﻿using MyDoctorOffice.Models;
using MyDoctorOffice.Service;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace MyDoctorOffice.Controllers
{
    [Authorize(Roles = "Patient")]
    public class PatientController : Controller
    {
        //TODO: Use DI
        PatientService _service = new PatientService();

        public ActionResult Index()
        {

            return View();
        }

        [HttpPost]
        public ActionResult Create(PatientModel patient) {
            if (ModelState.IsValid)
            {
                _service.CreatePatient(patient);


                return RedirectToAction("CreateConfirmation");
            }

            return View();
        }

        [HttpGet]
        public ActionResult Create()
        {
            return View();
        }

        public ActionResult CreateConfirmation()
        {
            return View();
        }
    }
}